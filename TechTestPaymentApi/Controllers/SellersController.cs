﻿using System.Net.Mime;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SellersController : ControllerBase
    {
        private readonly ILogger<SellersController> logger;
        private readonly ISellerRepository sellerRepository;

        public SellersController(ILogger<SellersController> logger, ISellerRepository sellerRepository)
        {
            this.logger = logger;
            this.sellerRepository = sellerRepository;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Seller>))]
        public async Task<IActionResult> Get_ActionResult()
        {
            try
            {
                var data = await sellerRepository.GetAsync();

                return Ok(data);
            }
            catch (Exception)
            {
                return BadRequest("Not possible to get now, try again later.");
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Seller))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById_ActionResult(int id)
        {
            try
            {
                var seller = await sellerRepository.GetByIdAsync(id);

                return Ok(seller);
            }
            catch (ArgumentOutOfRangeException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest("Not possible to get now, try again later.");
            }
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] Seller seller)
        {
            try
            {
                var newSeller = await sellerRepository.InsertAsync(seller);

                return Ok(newSeller);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(string.Format("Cannot be saved with the following data {0} ({1})", ex.ParamName, ex.Message));
            }
            catch (Exception)
            {
                return BadRequest("Not possible to save now, try again later.");
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await sellerRepository.DeleteAsync(id);

                return NoContent();
            }
            catch (ArgumentOutOfRangeException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest("Not possible to delete now, try again later.");
            }
        }
    }
}
