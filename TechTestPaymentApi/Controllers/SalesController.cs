using System.Net.Mime;
using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SalesController : ControllerBase
    {
        private readonly ILogger<SalesController> logger;
        private readonly ISaleRepository saleRepository;

        public SalesController(ILogger<SalesController> logger, ISaleRepository saleRepository)
        {
            this.logger = logger;
            this.saleRepository = saleRepository;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Sale>))]
        public async Task<IActionResult> Get_ActionResult()
        {
            try
            {
                var data = await saleRepository.GetAsync();

                return Ok(data);
            }
            catch (Exception)
            {
                return BadRequest("Not possible to get now, try again later.");
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Sale))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById_ActionResult(int id)
        {
            try
            {
                var sale = await saleRepository.GetByIdAsync(id);

                return Ok(sale);
            }
            catch (ArgumentOutOfRangeException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest("Not possible to get now, try again later.");
            }
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] SaleNewDTO sale)
        {
            try
            {
                var newSale = await saleRepository.CreateNewSale(sale);

                return Ok(newSale);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(string.Format("Cannot be saved with the following data {0} ({1})", ex.ParamName, ex.Message));
            }
            catch (Exception)
            {
                return BadRequest("Not possible to save now, try again later.");
            }
        }

        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put([FromBody] SaleUpdateDTO saleUpdate)
        {
            try
            {
                var newSale = await saleRepository.UpdateSale(saleId: saleUpdate.Id, newStatus: (ESaleStatus)saleUpdate.newStatus);

                return Ok(newSale);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(string.Format("Cannot be saved with the following data {0} ({1})", ex.ParamName, ex.Message));
            }
            catch (Exception)
            {
                return BadRequest("Not possible to update now, try again later.");
            }
        }




        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await saleRepository.DeleteAsync(id);

                return NoContent();
            }
            catch (ArgumentOutOfRangeException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest("Not possible to delete now, try again later.");
            }
        }
    }
}