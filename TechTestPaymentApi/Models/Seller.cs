﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Models
{
    public class Seller : IBaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public long DocumentNumber { get; set; }
        public string FormatedDocument => DocumentNumber.ToString();
        [Required]
        public string Email { get; set; }
        [Required]
        public long PhoneNumber { get; set; }
    }
}
