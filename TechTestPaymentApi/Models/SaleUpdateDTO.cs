﻿namespace TechTestPaymentApi.Models
{
    public class SaleUpdateDTO
    {
        public int Id { get; set; }
        public int newStatus { get; set; }
    }
}
