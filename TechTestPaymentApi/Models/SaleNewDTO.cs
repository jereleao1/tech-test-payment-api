﻿namespace TechTestPaymentApi.Models
{
    public class SaleNewDTO
    {
        public int SellerId { get; set; }
        public IEnumerable<int> ItemsIds { get; set; }
    }
}
