﻿using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Models
{
    public class Sale : IBaseModel
    {
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public DateTime Date { get; set; }
        public decimal TotalValue { get; set; }
        public IEnumerable<Item> Items { get; set; }
        public ESaleStatus Status { get; set; }
        public string StringStatus => Status.ToString();

    }

    public enum ESaleStatus
    {
        Cancelado = 1,
        AguardandoPagamento = 2,
        PagamentoAprovado = 3,
        EnviadoParaTransportadora = 4,
        Entregue = 5,
    }
}
