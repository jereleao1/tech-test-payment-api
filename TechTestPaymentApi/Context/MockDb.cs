﻿namespace TechTestPaymentApi.Context
{
    public class MockDb
    {
        private readonly ApiContext _context;

        public MockDb(ApiContext context)
        {
            _context = context;

            PopulateItens();
            PopulateSellers();
        }

        private void PopulateSellers()
        {
            var sellers = new List<Models.Seller>()
            {
                new Models.Seller()
                {
                    Name = "Luke Skywalker",
                    DocumentNumber = 09702853885,
                    Email = "jeremaios@gmail.com",
                    PhoneNumber = 11920135602,
                },
                new Models.Seller()
                {
                    Name = "Bioho Sugao",
                    DocumentNumber = 70888931859,
                    Email = "sugaosama@gmail.com",
                    PhoneNumber = 11920135602,
                },
                new Models.Seller()
                {
                    Name = "Paycu Zoys",
                    DocumentNumber = 55753172857,
                    Email = "paycubai@gmail.com",
                    PhoneNumber = 11920135602,
                },
                new Models.Seller()
                {
                    Name = "Leiun Peohoel",
                    DocumentNumber = 09702853885,
                    Email = "peoun_leiel@gmail.com",
                    PhoneNumber = 11920135602,
                },
            };

            sellers.ForEach(seller => _context.Sellers.Add(seller));

            _context.SaveChanges();        
        }

        private void PopulateItens()
        {
            var items = new List<Models.Item>()
            {
                new Models.Item()
                {
                    Name = "Martelo",
                    Price = 45M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Marreta",
                    Price = 60M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Cinto",
                    Price = 40.5M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Capacete",
                    Price = 89.99M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Alicate",
                    Price = 19.95M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Prego 15x20",
                    Price = 0.05M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Colher de Pedreiro",
                    Price = 35.9M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Lanterna",
                    Price = 10.99M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Luva",
                    Price = 5.85M,
                    IsActive = true,
                },
                new Models.Item()
                {
                    Name = "Martelo",
                    Price = 5M,
                    IsActive = true,
                },
            };

            items.ForEach(item => _context.Items.Add(item));

            _context.SaveChanges();
        }
    }
}
