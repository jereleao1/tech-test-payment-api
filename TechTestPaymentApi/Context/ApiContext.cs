﻿using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Context
{
    public class ApiContext : DbContext
    {
        public ApiContext() { }
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
            _ = new MockDb(this);
        }

        public virtual DbSet<Item> Items { get; set; }

        public virtual DbSet<Sale> Sales { get; set; }

        public virtual DbSet<Seller> Sellers { get; set; }
    }
}
