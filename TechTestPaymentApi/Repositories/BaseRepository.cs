﻿using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly ApiContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public BaseRepository(ApiContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }
        public IEnumerable<TEntity> Get()
        {
            return _dbSet.ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await _dbSet.ToListAsync();
        }
        public TEntity GetById(int id)
        {
            var entity = _dbSet.Find(id);

            if (entity is null) throw new ArgumentOutOfRangeException("Resource not found");

            return entity;
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            var entity = await _dbSet.FindAsync(id);

            if (entity is null) throw new ArgumentOutOfRangeException("Resource not found");

            return entity;
        }
        public TEntity Insert(TEntity entity)
        {
            _dbSet.Add(entity);

            Save();

            return entity;
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);

            await SaveAsync();

            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            _dbSet.Update(entity);

            Save();

            return entity;
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            _dbSet.Update(entity);

            await SaveAsync();

            return entity;
        }
        public void Delete(int id)
        {
            var entity = _dbSet.Find(id);

            if (entity is null) throw new ArgumentOutOfRangeException("Resource not found");

            _dbSet.Remove(entity);

            Save();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _dbSet.FindAsync(id);

            if (entity is null) throw new ArgumentOutOfRangeException("Resource not found");

            _dbSet.Remove(entity);

            await SaveAsync();
        }
        public void Save()
        {
            _dbContext.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
