﻿using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Repositories
{
    public class ItemRepository : BaseRepository<Item>, IItemRepository
    {
        public ItemRepository(ApiContext dbContext) : base(dbContext)
        {
        }
    }
}
