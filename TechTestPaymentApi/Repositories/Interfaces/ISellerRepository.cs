﻿using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Repositories.Interfaces
{
    public interface ISellerRepository : IBaseRepository<Seller>
    {
        Task<Seller> CreateNewSeller(Seller seller);
    }
}