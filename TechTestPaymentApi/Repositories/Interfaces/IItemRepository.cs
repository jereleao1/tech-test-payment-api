﻿using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Repositories.Interfaces
{
    public interface IItemRepository : IBaseRepository<Item>
    {
    }
}