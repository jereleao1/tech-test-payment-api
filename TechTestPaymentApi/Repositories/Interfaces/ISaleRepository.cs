﻿using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Repositories.Interfaces
{
    public interface ISaleRepository : IBaseRepository<Sale>
    {
        Task<Sale> CreateNewSale(SaleNewDTO sale);
        Task<Sale> UpdateSale(int saleId, ESaleStatus newStatus);
    }
}