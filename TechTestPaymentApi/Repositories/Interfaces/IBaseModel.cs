﻿using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Repositories.Interfaces
{
    public interface IBaseModel
    {
        public int Id { get; set; }
    }
}