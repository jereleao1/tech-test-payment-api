﻿using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Repositories.Interfaces
{
    public interface IBaseRepository<T>
    {
        IEnumerable<T> Get();
        Task<IEnumerable<T>> GetAsync();
        T GetById(int id);
        Task<T> GetByIdAsync(int id);
        T Insert(T entity);
        Task<T> InsertAsync(T entity);
        T Update(T entity);
        Task<T> UpdateAsync(T entity);
        void Delete(int id);
        Task DeleteAsync(int id);
        void Save();
        Task SaveAsync();
    }
}