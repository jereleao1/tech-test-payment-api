﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Repositories
{
    public class SellerRepository : BaseRepository<Seller>, ISellerRepository
    {
        public SellerRepository(ApiContext dbContext) : base(dbContext)
        {
        }

        public async Task<Seller> CreateNewSeller(Seller seller)
        {
            if (string.IsNullOrEmpty(seller.Name)) throw new ArgumentException("Name cannot be empty", "Name");

            if (Equals(seller.DocumentNumber, 0)) throw new ArgumentException("DocumentNumber cannot be empty", "DocumentNumber");

            if (string.IsNullOrEmpty(seller.Email)) throw new ArgumentException("Email cannot be empty", "Email");

            if (Equals(seller.PhoneNumber, 0)) throw new ArgumentException("PhoneNumber cannot be empty", "PhoneNumber");

            return await InsertAsync(seller);
        }
    }
}
