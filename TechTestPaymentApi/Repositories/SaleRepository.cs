﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Repositories.Interfaces;

namespace TechTestPaymentApi.Repositories
{
    public class SaleRepository : BaseRepository<Sale>, ISaleRepository
    {
        private readonly ISellerRepository _sellerRepository;
        private readonly IItemRepository _itemRepository;

        public SaleRepository(ApiContext dbContext, ISellerRepository sellerRepository, IItemRepository itemRepository) : base(dbContext)
        {
            _sellerRepository = sellerRepository;
            _itemRepository = itemRepository;
        }

        public async Task<Sale> CreateNewSale(SaleNewDTO sale)
        {
            if (Equals(sale.SellerId, 0)) throw new ArgumentException("SellerId cannot be equal to zero.");

            var seller = await _sellerRepository.GetByIdAsync(sale.SellerId);

            if (sale.ItemsIds.Count() == 0) throw new ArgumentException("At least one item must be selled to register a sale.");

            var itens = sale.ItemsIds.Select(id => _itemRepository.GetById(id)).ToList();

            Sale newSale = new Sale()
            {
                Seller = seller,
                Date = DateTime.Now,
                TotalValue = itens.Sum(i => i.Price),
                Items = itens,
                Status = ESaleStatus.AguardandoPagamento
            };

            return await InsertAsync(newSale);
        }

        public async Task<Sale> UpdateSale(int saleId, ESaleStatus newStatus)
        {
            if (Equals(saleId, 0)) throw new ArgumentException("saleId cannot be equal to zero.");

            var sale = await GetByIdAsync(saleId);

            if (sale.Status == ESaleStatus.AguardandoPagamento) 
            {
                if (newStatus != ESaleStatus.PagamentoAprovado &&
                    newStatus != ESaleStatus.Cancelado) throw new ArgumentException("Invalid new status.");
            } 
            else if (sale.Status == ESaleStatus.PagamentoAprovado)
            {
                if (newStatus != ESaleStatus.EnviadoParaTransportadora &&
                    newStatus != ESaleStatus.Cancelado) throw new ArgumentException("Invalid new status.");
            }
            else if (sale.Status == ESaleStatus.EnviadoParaTransportadora)
            {
                if (newStatus != ESaleStatus.Entregue) throw new ArgumentException("Invalid new status.");
            }

            sale.Status = newStatus;

            return await UpdateAsync(sale);
        }
    }
}
